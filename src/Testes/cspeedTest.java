package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import mikejyg.javaipacman.pacman.cspeed;

public class cspeedTest {
	/**
	 * speed control
	 * 
	 * use init(s,f) to set the frame/step ratio
	 * NOTE: s must <= f
	 * call start() to reset counters
	 * call isMove per frame to see if step are to be taken
	 */
	
	cspeed s;
	
	@Before
	public void setUp() throws Exception {
		s = new cspeed();
	}

// 	--------------- METODO isMove
	@Test
	public void testIsMoveOk() {
		s.start(1, 6);
		assertEquals("Está se movendo",1,s.isMove());
	}
	@Test
	public void testIsMoveNotOk() {
		s.start(0, 0);
		assertEquals("Está parado",0,s.isMove());
	}
}
