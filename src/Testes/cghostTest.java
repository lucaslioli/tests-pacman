package Testes;

import mikejyg.javaipacman.pacman.*;
import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

public class cghostTest {
	
	cghost ghost;
	cmaze cm;
	cpcman pac;
	
	@Before
	public void setUp() throws Exception {
		// cpcman � a classe main do jogo (paint, update, window...)
		pac = new cpcman(); 
		// cmaze � classe que define o labirinto (start, draw, drawDots)
		cm = new cmaze(pac, pac.offScreenG);
		// cghost � a classe respons�vel pelos fantasmas (start, draw, move)
		ghost = new cghost(pac, pac.offScreenG, cm, Color.red); 
		
		// Localiza��o do cghost na horizontal
		ghost.iX = 218;
		// Localiza��o do cghost na vertical
		ghost.iY = 80;
	}
	
// 	--------------- METODO testCollision
	@Test
	public void testTestCollision0() {
		/*
		 * Com o ghost na posi��o (218,80) e o cpac na posicao (200,40)
		 * nenhum dos dois morre, mesmo com o fantasma no estado do ghost = 1 (fora da ninho)
		 * pois a dist�ncia entre eles � > do que 2px (definido via c�digo)
		 */
		ghost.iStatus = 1; // OUT
		assertEquals(0, ghost.testCollision(200, 40));// 0 - No one Die
	}
	
	@Test
	public void testTestCollision1() {
		/*
		 * Com o ghost na posi��o (218,80) e o cpac na MESMA posi��o
		 * um deles morre, pois a dist�ncia entre eles � <= do que 2px
		 * e como o estado do ghost � = 1 (fora da ninho) o pac morre 
		 */
		ghost.iStatus = 1; // OUT
		assertEquals(1, ghost.testCollision(218,80)); // 1 - Pac Die
	}
	
	@Test
	public void testTestCollision2() {
		/*
		 * simula o mesmo caso do teste anterior (testTestCollision1) 
		 * por�m como o ghost est� com o estado = 2 (cego) o ghost morre  
		 */
		ghost.iStatus = 2; // BLIND
		assertEquals(2, ghost.testCollision(218,80)); // 2 - Ghost Die
	}
	
// 	--------------- OBJETO cmaze
	@Test
	public void testObjectNotSame(){
		/*  
		 * pac.maze e cm s�o Objetos do mesmo tipo, mas n�o iguais;
		 * pac.maze � uma vari�vel do Objeto cmaze n�o instanciada, ent�o seu valor � nulo
		 */
		assertNotSame("Objetos diferentes", pac.maze, cm);
	}
	
	@Test
	public void testNullException(){
		/*
		 * pac.maze � uma vari�vel do Objeto cmaze n�o instanciada
		 * ent�o espera-se seu valor seja nulo
		 */
		assertNull("Objeto NULL", pac.maze);
	}
	
	@Test
	public void testNullExceptionFalse(){
		/*
		 * cm � um objeto que foi inst�nciado de cmaze
		 * ent�o espera-se que n�o seja null
		 */
		assertNotNull(cm);
	}
}
