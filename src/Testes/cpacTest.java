package Testes;

import static org.junit.Assert.*;

//import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import mikejyg.javaipacman.pacman.*;

public class cpacTest {

	cpac pac;
	cmaze cm;
	cpcman pc;
	
	@Before
	public void setUp() throws Exception {
		// cpcman � a classe main do jogo (paint, update, window...)
		pc = new cpcman(); 
		// cmaze � classe que define o labirinto (start, draw, drawDots)
		cm = new cmaze(pc, pc.offScreenG);
		pc.initImages();
		// cpac � classe respons�vel pelo personaem do jogo (start, draw, move)
		pac = new cpac(pc, pc.offScreenG, cm, pc.powerDot);
		pac.draw();
		pac.start();
		pc.paint(pc.offScreenG);
	}
// 	--------------- METODO mazeOk
	@Test
	public void testMazeOKTrue() {
		/*
		 *  Verifica se a posi��o existe no labirinto
		 *  No caso, est� sendo passada uma posi��o Ok
		 *  espera-se que o retorno seja TRUE
		 *  mazeOK(row, col) -> iMaze[col][row]
		 */
		assertTrue(pac.mazeOK(10,10));
	}

// 	--------------- METODO moveEatenDot
	@Test
	public void testMoveEatenDot() {
		/*
		 * Pac est� na posi��o [192][160] ou [12][10]
		 * na qual existe um DOT (verificado por teste MANUAL via c�digo);
		 */
		pac.iX = 176;
		pac.iY = 160;
		pac.iDir = 0;
		assertEquals(1, pac.move(pac.iDir));
	}
	
	@Test
	public void testMoveEatenPowerDot() {
		/*
		 * Pac est� na posi��o [304][196] ou [19][12]
		 * na qual existe um POWER_DOT (verificado por teste MANUAL via c�digo);
		 */
		pac.iX = 304;
		pac.iY = 192;
		pac.iDir = 3;
		assertEquals(2, pac.move(pac.iDir));
	}

}
