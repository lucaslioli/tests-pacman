package Testes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ cghostTest.class, cpacTest.class, cspeedTest.class })
public class TestSuite {

}
